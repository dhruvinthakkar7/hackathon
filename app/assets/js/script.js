$(document).ready(function(){
    $("#course-items").owlCarousel({
        items: 3,
        autoplay: true,
        margin: 20,
        loop: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: true
    });
  });

  $(document).ready(function(){
    $("#teachers").owlCarousel({
        items: 3,
        autoplay: true,
        margin: 20,
        loop: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: true
    });
  });